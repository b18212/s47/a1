const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);

const spanFullName = document.querySelector("#span-full-name");

function firstLastName() {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener("keyup", firstLastName);
txtLastName.addEventListener("keyup", firstLastName);